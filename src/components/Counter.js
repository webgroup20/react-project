import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { increment } from '../features/counter/counterSlice'

export default function Counter() {

    // request action ( order )
    const dispatch = useDispatch()

    // recieved global state
    const {count} = useSelector((state) => state.counter)

  return (
    <main className='mt-20'>
        <h1 className='text-4xl'>Today class, RTK</h1>
        <button
            onClick={() => dispatch(increment())}
            >Increment</button>
        <p>You clicked {count} times</p>
    </main>
  )
}
