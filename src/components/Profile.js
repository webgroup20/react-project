// create Profile component
function Profile(){
    return(
        <div className="w-64">
            <img 
                className="h-auto max-w-full rounded-lg"
                src="https://flowbite.s3.amazonaws.com/docs/gallery/square/image-8.jpg" 
                alt="avatar" 
            />
        </div>
    )
}
export function Gallery(){
    return(
        <>
            <Profile />
            <Profile />
            <Profile />
            <Profile />
        </>
    )
}

// --- Object Styles ----
const avatar = {
    borderRadius: 10
}
const heading = {
    color: 'darkblue'
}
const title = {
    textTransform: 'uppercase'
}
