import React, { useEffect, useState } from 'react'
import DataTable from 'react-data-table-component';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { deleteProduct, searchProducts } from '../../features/products/createAction';

export default function ProductDataTable() {
    const navigate = useNavigate()
    const columns = [
        {
            name: 'ID',
            selector: row => <div>{row.id}</div>,
            sortable: true
        },
        {
            name: 'Product Name',
            selector: row => row.title,
            sortable: true
        },
        {
            name: 'Price',
            selector: row => `$ ${row.price}`,
            sortable: true
        },
        {
            name: 'Thumbnail',
            selector: row => <img className='w-20 p-3' src={row.images[0]} alt={row.title} />
        },
        {
            name: 'Category',
            selector: row => row.category.name,
            sortable: true
        },
        {
            name: 'Action',
            selector: row => <div className='w-[300px]'>
                <button className='text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800'>View</button>
                <button 
                    onClick={(e) => {
                        navigate("/product/edit", {
                            state: row
                        })
                    }}
                    className='text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800'>Edit</button>
                <button 
                    onClick={() => {
                        handleDelete(row.id)
                    }}
                    className='text-white bg-red-600 hover:bg-red-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800'>Delete</button>
            </div>
        }
    ];
    
    //const [products, setProducts] = useState([]) // local state

    // received global state
    const {products, status, error} = useSelector(state => state.products)
    const [query, setQuery] = useState("")

    // request to reducer ( productSlice )
    const dispatch = useDispatch()

    useEffect(()=>{
        dispatch(searchProducts(query))
    }, [query, products])

    const handleDelete = (id) => {
        let isDelete = window.confirm("Are you sure to delete?")
        if (isDelete) {
            dispatch(deleteProduct(id))
        }
    }
    
  return (
    <section className='mt-20 overflow-scroll'>
        <DataTable 
            columns={columns}
            data={products}
            pagination
            subHeader
            subHeaderComponent={
                <form class="w-1/2 mx-auto">   
                    <label for="default-search" class="mb-2 text-sm font-medium text-gray-900 sr-only dark:text-white">Search</label>
                    <div class="relative">
                        <div class="absolute inset-y-0 start-0 flex items-center ps-3 pointer-events-none">
                            <svg class="w-4 h-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m19 19-4-4m0-7A7 7 0 1 1 1 8a7 7 0 0 1 14 0Z"/>
                            </svg>
                        </div>
                        <input 
                            onChange={(e) => {
                                console.log(e)
                                setQuery(e.target.value)
                            }}
                            type="search" id="default-search" class="block w-full p-4 ps-10 text-sm text-gray-900 border border-gray-300 rounded-lg bg-gray-50 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Search Mockups, Logos..." required />
                        <button type="submit" class="text-white absolute end-2.5 bottom-2.5 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Search</button>
                    </div>
                </form>

            }
        />
    </section>
  )
}
