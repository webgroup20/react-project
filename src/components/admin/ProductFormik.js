import React, { useEffect, useState } from 'react'
import { fetchCategories, insertProduct, updateProduct, uploadImageToServer } from '../../services/productAction';
import { useFormik } from 'formik';
import * as yup from 'yup'
import { useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { postProduct } from '../../features/products/createAction';

export default function ProductFormik({isEditing}) {

    const location = useLocation()

    // request action post product
    const dispatch = useDispatch()

    const formik = useFormik({
        initialValues: {
            id: 0,
            title: "",
            price: 0,
            description: "",
            categoryId: "", 
            images: ""
        },
        validationSchema: yup.object({
            title: yup.string()
                        .required("title cannot be blank")
                        .min(5),
            price: yup.number()
                        .required("Please enter product price")
                        .positive()
                        .max(1_000_000),
            description: yup.string()
                            .required("Please enter description")
                            .min(5),
            categoryId: yup.string()
                            .required("Please choose category"),
            // images: yup.mixed()
            //                 .required("Please choose any files...")
            //                 .test('FILE_SIZE', "File size is too big...", (value) => value && value.size < 10*1024*1024)
            //                 .test("FILE_TYPE", "Invalid file format...", 
            //                  (value) => value && ['image/png', 'image/jpg', 'image/jpeg'].includes(value.type))
         

        }),
        onSubmit: (values) => {
            console.log('source image: ', source); console.log('is user want to edit? ', isEditing);
            if(isEditing){
                // ----- user want to edit content //----- is user browse new image?
                if (source == ""){
                    // --- user don't browse new image ---
                    console.log('object product: ', values);
                    updateProduct(values)
                    .then(res => {
                        console.log('update product without new image', res);
                    })
                }else{
                    /// --- user browse new image for update product ---
                    console.log('broswer new image', source);
                    const image = new FormData()
                    image.append('file', source)
                    uploadImageToServer(image)
                    .then((res) => {
                        console.log('image response: ', res.data.location);
                        values.images = [res.data.location]
                        console.log('value', values);
                        /// ----- update product with image ---
                        updateProduct(values)
                        .then(res => {
                            console.log('sucess update product with new image', res);
                        })
                    })
                }
            }else{
                // insert product
                values.images = ["https://eduport.webestica.com/assets/images/courses/4by3/07.jpg"]
                console.log('before insert product', values);
                dispatch(postProduct(values))
            }
            
        }
    })

    // declare state
    const [categories, setCategories] = useState([])
    const [source, setSource] = useState("")
    useEffect(() => {
        fetchCategories()
        .then(response => {
            setCategories(response)
        })

        console.log('is editing', isEditing);
        if (isEditing) {
            // it willl execute when user is editing on form
            console.log(location.state);
            /// destructing object
            const {id, title, price, category, description, images} = location.state
            const {values} = formik
            
            // re-assign to each element of formik 
            values.title = title
            values.price = price
            values.description = description
            values.images = images
            values.categoryId = category.id
            values.id = id
            console.log('image from navigation', values.images);
        }
    }, [])

  return (
    <section className="bg-white dark:bg-gray-900 mt-10">
        <div className="py-8 px-4 mx-auto max-w-2xl lg:py-16">
            <h2 className="mb-4 text-2xl font-bold text-gray-900 dark:text-white text-center">Add a new product</h2>
            <form onSubmit={formik.handleSubmit}>
                <div className="grid gap-4 sm:grid-cols-2 sm:gap-6">
                    <div className="sm:col-span-2">
                        <label for="title" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Product Name</label>
                        <input
                            onChange={formik.handleChange}
                            value={formik.values.title}
                            type="text" name="title" id="title" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500" placeholder="Type product name" required="" />
                        {
                            formik.errors.title && 
                            <p className='text-red-600'>{formik.errors.title}</p>
                        }
                    </div>
                    
                    <div className="w-full">
                        <label for="price" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Price</label>
                        <input
                            onChange={formik.handleChange} 
                            value={formik.values.price}
                            type="number" name="price" id="price" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500" placeholder="$2999" required="" />
                        {
                            formik.errors.price && 
                            <p className='text-red-600'>{formik.errors.price}</p>
                        }
                    </div>
                    <div>
                        <label for="category" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Category</label>
                        <select
                            onChange={formik.handleChange}
                            value={formik.values.categoryId}
                            name="categoryId" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500">
                            <option value="">Select category</option>
                            
                            {
                                categories.map((cat, index) => 
                                <option key={index} value={cat.id}>{cat.name}</option>)
                            }
                        </select>
                        {
                            formik.errors.categoryId && <p className='text-red-600'>{formik.errors.categoryId}</p>
                        }
                    </div> 
                    <div className="sm:col-span-2">
                        <label for="description" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Description</label>
                        <textarea 
                            onChange={formik.handleChange}
                            value={formik.values.description}
                            name="description" rows="8" className="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-primary-500 focus:border-primary-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500" placeholder="Your description here"></textarea>
                        {
                            formik.errors.description && <p className='text-red-600'>{formik.errors.description}</p>
                        }
                    </div>

                    {/* browse images */}
                    <div className="flex items-center justify-center w-full">
                        <label for="dropzone-file" className="flex flex-col items-center justify-center w-full h-64 border-2 border-gray-300 border-dashed rounded-lg cursor-pointer bg-gray-50 dark:hover:bg-gray-800 dark:bg-gray-700 hover:bg-gray-100 dark:border-gray-600 dark:hover:border-gray-500 dark:hover:bg-gray-600">
                            <div className="flex flex-col items-center justify-center pt-5 pb-6">
                                <svg className="w-8 h-8 mb-4 text-gray-500 dark:text-gray-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 16">
                                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 13h3a3 3 0 0 0 0-6h-.025A5.56 5.56 0 0 0 16 6.5 5.5 5.5 0 0 0 5.207 5.021C5.137 5.017 5.071 5 5 5a4 4 0 0 0 0 8h2.167M10 15V6m0 0L8 8m2-2 2 2"/>
                                </svg>
                                <p className="mb-2 text-sm text-gray-500 dark:text-gray-400"><span className="font-semibold">Click to upload</span> or drag and drop</p>
                                <p className="text-xs text-gray-500 dark:text-gray-400">SVG, PNG, JPG or GIF (MAX. 800x400px)</p>
                            </div>
                            <input 
                                onChange={(e) => {
                                    // validate images whether it pass the .test
                                    formik.setFieldValue("images", e.target.files[0])
                                    // setSource for preview
                                   setSource(e.target.files[0])
                                }}
              
                                id="dropzone-file" type="file" className="hidden" />
                            </label>
                            
                        </div> 
                        <div>
                            {/* <img src={formik.values.images.length < 0 ? "https://eduport.webestica.com/assets/images/courses/4by3/08.jpg" : } alt="" /> */}
                            <img src={source == "" ? formik.values.images[0] : URL.createObjectURL(source)} alt="" />
                        </div>
                        {
                            formik.errors.images && <p className='text-red-500'>{formik.errors.images}</p>
                        }
                    
                </div>
                <button type="submit" className="inline-flex items-center px-5 py-2.5 mt-4 sm:mt-6 text-sm font-medium text-center text-white bg-primary-700 rounded-lg focus:ring-4 focus:ring-primary-200 dark:focus:ring-primary-900 hover:bg-primary-800">
                    {
                        isEditing ? "Update Product" : "Add Product"
                    }
                </button>
            </form>

            
        </div>
        
    </section>
  )
}
