import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Navigation, Pagination } from "swiper/modules";
import "./../App.css"
import "swiper/css";
import "swiper/css/pagination";
import 'swiper/css/navigation';

import { Cards, ProductCard } from "./Card";
export default function MeySwiper({products}) {
  return (
    <>
      <Swiper
        slidesPerView={4}
        spaceBetween={10}
        modules={[Pagination, Autoplay, Navigation]}
        autoplay={{
            delay: 3000,
            disableOnInteraction: false
        }}
        navigation={true}
      > 
        {
            products.map((product, index) => <SwiperSlide>
                <ProductCard
                    key={index} 
                    data={product} 
                />
            </SwiperSlide>)
        }  
      </Swiper>
    </>
  );
}
