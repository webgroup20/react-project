import { useFormik } from 'formik';
import * as yub from "yup";
import React, { useEffect } from 'react';
import HideShowPassword from './HideShowPassword';
import { useDispatch, useSelector } from 'react-redux';
import { userLogin } from '../features/auth/authAction';
import { redirect, useNavigate } from 'react-router-dom';

export default function LoginForm() {
    const passwordRules = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{5,}$/;
    const pwdRules = /[a-z]/;

    const navigate = useNavigate()

    const dispatch = useDispatch()
    const {success, error} = useSelector(state => state.auth)

     // redirect authenticated user to profile screen
    useEffect(() => {
        if(success) {
            navigate('/dashboard')
        }
    }, [navigate, success])

    const formik = useFormik({
        initialValues: {
            email: "",
            password: ""
        },
        validationSchema: yub.object({
            email: yub.string()
                      .required("Please enter email")
                      .email("Invalid email address"),
            password: yub.string().required("Password is required")
                         .min(8)
                         .matches(pwdRules, {message: "Please create a strong password"})
        }),
        onSubmit: (values) => {
            console.log(values);
            dispatch(userLogin(values))
        }
    })

    const {handleChange, handleSubmit, errors, values} = formik

  return (
    <section className="bg-gray-50 dark:bg-gray-900">  
      <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
        <a
          href="/"
          className="flex items-center mb-6 text-2xl font-semibold text-gray-900 dark:text-white"
        >
          Web Design Class
        </a>
        <div className="w-full bg-white rounded-lg shadow dark:border md:mt-0 sm:max-w-md xl:p-0 dark:bg-gray-800 dark:border-gray-700">
          <div className="p-6 space-y-4 md:space-y-6 sm:p-8">
            <h1 className="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl dark:text-white">
              Sign in to your account
            </h1>
            {/* unauthorized message */}
            {error && (
              <p className="text-center text-red-600">{error}</p>
            )}
            <form
              className="space-y-4 md:space-y-6"
              onSubmit={handleSubmit}
            >
              <div>
                <label
                  htmlFor="email"
                  className="block mb-2 text-sm font-medium text-gray-900 dark:text-white"
                >
                  Your email
                </label>
                <input
                  type="email"
                  name="email"
                  onChange={handleChange}
                  value={values.email}
                  className="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="name@company.com"
                  required=""
                />
                {
                  errors.email ? <p className="text-red-700">{errors.email}</p> : null
                }
              </div>
              <div className="relative">
                <HideShowPassword
                    value={values.password} 
                    onChange={handleChange}
                    errorMessage={errors.password}
                    labelMessage="Password" 
                    name="password" 
                />
              </div>
              <div className="flex items-center justify-between">
                <div className="flex items-start">
                  <div className="flex items-center h-5">
                    <input
                      id="remember"
                      aria-describedby="remember"
                      type="checkbox"
                      className="w-4 h-4 border border-gray-300 rounded bg-gray-50 focus:ring-3 focus:ring-primary-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-primary-600 dark:ring-offset-gray-800"
                      required=""
                    />
                  </div>
                  <div className="ml-3 text-sm">
                    <label
                      htmlFor="remember"
                      className="text-gray-500 dark:text-gray-300"
                    >
                      Remember me
                    </label>
                  </div>
                </div>
                <a
                  href="/"
                  className="text-sm font-medium text-primary-600 hover:underline dark:text-primary-500"
                >
                  Forgot password?
                </a>
              </div>
              <button
                type="submit"
                className="w-full text-white bg-primary-600 hover:bg-primary-700 focus:ring-4 focus:outline-none focus:ring-primary-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800"
              >
                Sign in
              </button>
              <p className="text-sm font-light text-gray-500 dark:text-gray-400">
                Don’t have an account yet?{" "}
                <a
                  href="#"
                  className="font-medium text-primary-600 hover:underline dark:text-primary-500"
                >
                  Sign up
                </a>
              </p>
            </form>
          </div>
        </div>
      </div>
    </section>

  )
}
