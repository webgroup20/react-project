import { useEffect, useState } from "react";
import { Loading } from "../components/Loading";
import { fetchProducts } from "../services/productAction";
import { ProductCard } from "../components/Card";

export function Products(){
    const [products, setProducts] = useState([])
    const [loading, setLoading] = useState(true);
    useEffect(() => {
        // data fetching from remote server
        fetchProducts()
        .then(response => {
            setProducts(response)
            setLoading(false)
        })
    }, [])
    return(
        <main className=" container mx-auto">
            <section className="grid grid-cols-1 md:grid-cols-2 xl:grid-cols-4 gap-3">
            {
                loading ? 
                <Loading /> 
                : 
                products.map((product, index) => 
                    <ProductCard 
                        key={index} 
                        data={product} 
                    />)
            }
            </section>
        </main>
    )
}