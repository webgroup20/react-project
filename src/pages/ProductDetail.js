import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom';
import { fetchOneProduct } from '../services/productAction';
export default function ProductDetail() {
  const {id} = useParams()
  console.log(id);
  // declare state
  const [product, setProduct] = useState({})

  useEffect(() => {
    // data fetching | subscription
    fetchOneProduct(id)
    .then((response) => {
      console.log(response);
      setProduct(response)
    })
  }, [])

  return (
    <main>
      <section>
        <p>
          helo
        </p>
      </section>
      <section className='mt-10'>ProductDetail: 
        <h2 className='text-4xl text-center'>{product && product.title}</h2>
      </section>
    </main>
  )
}
