import { Navigate, useNavigate } from "react-router-dom";
import ProductDataTable from "../components/admin/ProductDataTable";
import { useSelector } from "react-redux";
import { useEffect } from "react";

export function Dashboard(){
   
    const {userToken, isLoggedIn} = useSelector(state => state.auth)

    useEffect(() => {
        console.log('isLogin dashboard', isLoggedIn);
        console.log('token dashboard', userToken);
    }, [isLoggedIn, userToken])

    return isLoggedIn ? (
        <main className="p-4 md:ml-64 h-auto pt-20">
                {/* call data table */}
                <ProductDataTable />
        </main>
    ) : <Navigate to={"/login"} />
}