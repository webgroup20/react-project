import axios from "axios";

const BASE_URL = "https://api.escuelajs.co/api/v1/";
export async function fetchProducts(){
    const response = await fetch(`${BASE_URL}products`)
    return response.json()
}
// search product by title
export async function searchProducts(query){
    const response = await fetch(`${BASE_URL}products?title=${query}`)
    return response.json()
}

// get one products ( detail products )
export async function fetchOneProduct(id){
    const response = await fetch(`${BASE_URL}products/${id}`)
    return response.json()
}

/// get categories
export async function fetchCategories(){
    const response = await fetch(`${BASE_URL}categories`)
    return response.json()
}

/// insert product to server
export async function insertProduct(product){
    const response = await fetch(`${BASE_URL}products`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(product)
    })
    return response.json()
}

export async function updateProduct(product){
    const response = await fetch(`${BASE_URL}products/${product.id}`, {
        method: "PUT",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify(product)
    })
    return response.json()
}

/// upload image to server
export async function uploadImageToServer(image){
    const response = await axios(`${BASE_URL}files/upload`, {
        method: "POST",
        headers: {
            "Content-Type": "multipart/form-data"
        },
        data: image
    })
    return response
}