export const users = [
    {
        "image": "https://flowbite.com/docs/images/people/profile-picture-5.jpg",
        "name": "Senglong",
        "position": "Designer"
    },
    {
        "image": "https://flowbite.com/docs/images/people/profile-picture-1.jpg",
        "name": "Chhaya",
        "position": "Manager"
    },
    {
        "image": "https://flowbite.com/docs/images/people/profile-picture-2.jpg",
        "name": "Samnang",
        "position": "Developer"
    },
    {
        "image": "https://flowbite.com/docs/images/people/profile-picture-4.jpg",
        "name": "Pov",
        "position": "Gamer"
    }
]