import logo from './logo.svg';
// import './App.css';
import { Gallery } from './components/Profile';
import Card, { ProfileCard } from './components/Card';
import { initFlowbite } from 'flowbite';
import { users } from './data/users';
import Home from './pages/Home';
import { Outlet, Route, Routes } from 'react-router-dom';
import { About } from './pages/About';
import { Products } from './pages/Products';
import { Login } from './pages/Login';
import Navbar from './components/Navbar';
import Footer from './components/Footer';
import ProductDetail from './pages/ProductDetail';
import NotFound from './pages/NotFound';
import { Dashboard } from './pages/Dashboard';
import AdminNavbar from './components/admin/AdminNavbar';
import LeftSideBar from './components/admin/LeftSideBar';
import ProductForm from './components/admin/ProductForm';
import ProductFormik from './components/admin/ProductFormik';
import Counter from './components/Counter';

// Import Swiper styles

import { useEffect } from 'react';

import AOS from 'aos';
import 'aos/dist/aos.css'
import secureLocalStorage from 'react-secure-storage';


function App() {
  useEffect(() => {
    if (!secureLocalStorage.getItem('color-theme')){
      // set default theme to light
      secureLocalStorage.setItem('color-theme', 'light')
    }
    if (secureLocalStorage.getItem('color-theme') === 'dark' || (!('color-theme' in secureLocalStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
      document.documentElement.classList.add('dark')
    }else{
      document.documentElement.classList.remove('dark')
    }

  }, [])
  return (
    <Routes>
      <Route 
        path='/'
        element={<MainLayout />}>
          {/* place children */}
          <Route 
            path='/'
            element={<Home />}
          />
          <Route 
            path='/about'
            element={<About />}
          />
          <Route 
            path='/products'
            element={<Products />}
          />
          <Route 
            path='/counter'
            element={<Counter />}
          />
          <Route 
            path='/products/:id'
            element={<ProductDetail />}
          />
          {/* end children */}
        </Route>
      
      <Route 
        path='/login'
        element={<Login />}
      />

      {/* ======admin layout======== */}
      <Route
        path='/'
        element={<AdminLayout />}
      >
          <Route 
            path='/dashboard'
            element={<Dashboard />}
          />
          <Route 
            path='/product/create'
            element={<ProductFormik isEditing={false} />}
          />
          <Route 
            path='/product/edit'
            element={<ProductFormik isEditing={true} />}
          />
      </Route>
      {/* =====end admin layout ==== */}
      
      <Route 
        path='*'
        element={<NotFound />}
      />
    </Routes>
  );
}

export default App;

// create main layout
function MainLayout(){
  useEffect(() => {
    AOS.init();
  }, [])
  return(
    <>
      <Navbar />
        <Outlet />
      <Footer />
    </>
  )
}

// create admin layout
function AdminLayout(){
  return(
    <>
      <AdminNavbar />
      <LeftSideBar />
      <Outlet />
    </>
  )
}
