import { createSlice } from "@reduxjs/toolkit";
import { getProfile } from "./profileAction";

export const profileSlice = createSlice({
    name: 'profile',
    initialState: {
        loading: true,
        profile: {},
        error: null,
        messageError: {}
    },
    extraReducers(builder){
        builder.addCase(getProfile.pending, (state) => {
            state.loading = true
        }).addCase(getProfile.fulfilled, (state, {payload}) => {
            state.profile = payload
            state.loading = false
            console.log('profile', payload);
        }).addCase(getProfile.rejected, (state, action) => {
            state.error = true
            state.messageError = action.error
            console.log('error get profile', action.error);
        })
    }
})

export default profileSlice.reducer