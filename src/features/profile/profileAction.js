import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { BASE_URL } from "../api";

export const getProfile = createAsyncThunk('/user/profile', async (token) => {
    try {
        const response = axios(`${BASE_URL}auth/profile`, {
            headers: {
                "Authorization": `bearer ${token}`
            }
        })
        const {data} = await response
        return data
    } catch (error) {
        return Promise.reject(error)
    }
})