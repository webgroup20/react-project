import { createSlice } from "@reduxjs/toolkit"
import secureLocalStorage from "react-secure-storage"
import { authlogout, userLogin } from "./authAction"
import { redirect } from "react-router-dom";

// initialize userToken from local storage
const userToken = secureLocalStorage.getItem('userToken')

const initialState = userToken ? 
    { 
        isLoggedIn: true, 
        userToken,
        success: true
    } : 
    { 
        isLoggedIn: false, 
        userToken: null,
        success: false, 
    };

  const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        logout: (state) => {
            secureLocalStorage.removeItem('userToken') // deletes token from storage
            state.userToken = null
            console.log('logout');
            console.log('user token in logout', userToken);
            state.isLoggedIn = false
        }
    },
    extraReducers(builder) {
        builder.addCase(userLogin.pending, (state) => {
            state.success = false
        }).addCase(userLogin.fulfilled, (state, {payload}) => {
            state.userToken = payload.access_token
            state.success = true
            console.log('done login', userToken);
            console.log("user info", payload)
            state.isLoggedIn = true
        }).addCase(userLogin.rejected, (state, {payload}) => {
            console.log('login reject', payload);
            state.isLoggedIn = false
        })
    },
  })

  export const {logout} = authSlice.actions
  
  export default authSlice.reducer