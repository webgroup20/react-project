import { createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";
import { BASE_URL } from "../api";
import secureLocalStorage from "react-secure-storage";
// create login action
export const userLogin = createAsyncThunk('/user/login', async(user, {rejectWithValue}) => {
    try {
        const response = axios(`${BASE_URL}auth/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            data: JSON.stringify(user)
        })
        const {data} = await response
        secureLocalStorage.setItem('userToken', data.access_token)
        console.log('data token', data.access_token);
        return data
    } catch (error) {
        return rejectWithValue(error.message)
    }
})

