// contain all action of asyncThunk 

import { createAsyncThunk } from "@reduxjs/toolkit";
import { BASE_URL } from "../api";

// function get all products
export const fetchProducts = createAsyncThunk('/products/fetchProducts', async () => {
    try{
        const response = await fetch(`${BASE_URL}products`)
        const data = await response.json()
        console.log('RTK', data);
        return data
    }catch(error){
        return Promise.reject(error)
    }
})

// search product by title
export const searchProducts = createAsyncThunk('/products/searchProducts', async (query) => {
    try{
        const response = await fetch(`${BASE_URL}products?title=${query}`)
        const data = await response.json()
        return data
    }catch(error){
        return Promise.reject(error)
    }
})

// delete product by id
export const deleteProduct = createAsyncThunk('/products/deleteProduct', async (id) => {
    try{
        const response = await fetch(`${BASE_URL}products/${id}`, {
            method: "DELETE"
        })
        const data = await response.json()
        console.log('sucess delete, ', data);
        console.log('delete product id: ', id);
        return id
    }catch(error){
        return Promise.reject(error)
    }
})

export const postProduct = createAsyncThunk('/products/postProduct', async (product) => {
    try{
        const response = await fetch(`${BASE_URL}products`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify(product)
        })
        const data = await response.json()
        return data

    }catch(error){
        return Promise.reject(error)
    }
})