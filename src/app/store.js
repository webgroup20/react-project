import { configureStore } from '@reduxjs/toolkit'
import counterReducer from '../features/counter/counterSlice'
import productReducer from '../features/products/productSlice'
import authReducer from '../features/auth/authSlice'
import profileReducer from '../features/profile/profileSlice'

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    products: productReducer,
    auth: authReducer,
    profile: profileReducer
  },
})